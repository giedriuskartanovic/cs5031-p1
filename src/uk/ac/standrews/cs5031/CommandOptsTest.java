package uk.ac.standrews.cs5031;

import static org.junit.Assert.*;

import org.junit.Test;

public class CommandOptsTest {

    @Test
    public void optionsTest() {
        String[] args = {"--guesses", "2", "--hints", "4", "words.txt"};
        CommandOpts opts = new CommandOpts(args);
        assertEquals(opts.maxGuesses, 2);
        assertEquals(opts.maxHints, 4);
        assertEquals(opts.wordSource, "words.txt");

        // Test it works with different ordering of arguments
        String[] argsDiff = {"words.txt", "--guesses", "3", "--hints", "5"};
        opts = new CommandOpts(argsDiff);
        assertEquals(opts.maxGuesses, 3);
        assertEquals(opts.maxHints, 5);
        assertEquals(opts.wordSource, "words.txt");
    }

}
