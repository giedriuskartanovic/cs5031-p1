package uk.ac.standrews.cs5031;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * The internal engine of the game that keeps the word, guesses,
 * hints, guessed letters and checks for winning/losing conditions.
 */
class GameState {
    private String word;
    private int guessesMade;
    private int guessesLeft;
    private int hintsLeft;

    private ArrayList<Character> guessedLetters;
    private ArrayList<Character> unguessedLetters;

    private Scanner sc = new Scanner(System.in).useDelimiter("\n");

    /**
     * @param word       Word to guess
     * @param maxGuesses Number of wrong guesses that the player can have
     * @param maxHints   Number of hints that the player can have
     */
    GameState(String word, int maxGuesses, int maxHints) {
        this.word = word;
        unguessedLetters = new ArrayList<>();
        guessedLetters = new ArrayList<>();

        // Add all letters to unguessedLetters while avoiding duplicates
        for (int i = 0; i < word.length(); ++i) {
            if (!unguessedLetters.contains(Character.toLowerCase(word.charAt(i))))
                unguessedLetters.add(Character.toLowerCase(word.charAt(i)));
        }

        guessesMade = 0;
        guessesLeft = maxGuesses;
        hintsLeft = maxHints;
    }

    /**
     * Print out the word with unguessed characters hidden
     */
    void showWord() {
        for (int i = 0; i < word.length(); ++i) {
            if (guessedLetters.contains(Character.toLowerCase(word.charAt(i)))) {
                System.out.print(word.charAt(i));
            } else {
                System.out.print("-");
            }
        }
        System.out.println("");
    }

    /**
     * @param letter Player's guessed character
     * @return Whether player's guess was correct or not
     */
    boolean guessLetter(char letter) {
        int i;

        if (letter == '?') {
            showHint();
            return false;
        }

        guessesMade++;

        // Go over unguessed letters and see if any of them match user's guess
        for (i = 0; i < unguessedLetters.size(); ++i) {
            if (unguessedLetters.get(i) == letter) {
                unguessedLetters.remove(i);
                guessedLetters.add(letter);
                return true;
            }
        }

        guessesLeft--;
        return false;
    }

    /**
     * @return Whether the player has won
     */
    boolean won() {
        return unguessedLetters.size() == 0;
    }

    /**
     * @return Whether the player has lost
     */
    boolean lost() {
        return unguessedLetters.size() > 0 && guessesLeft == 0;
    }

    /**
     * Print out a hint for the player (a possible letter that will be correct).
     * Prints out a warning if there are no more guesses left.
     */
    void showHint() {
        if (hintsLeft == 0) {
            System.out.println("No more hints allowed");
            return;
        }

        hintsLeft--;

        System.out.print("Try: ");
        System.out.println(unguessedLetters.get((int) (Math.random() * unguessedLetters.size())));
    }

    /**
     *
     */
    void showGuessesRemaining() {
        System.out.println("Guesses remaining: " + guessesLeft);
    }

    boolean getNextGuess() {
        String inputString;

        System.out.print("Guess a letter or word" + (hintsLeft > 0 ? " (? for a showHint)" : "") + ":");

        inputString = sc.next().toLowerCase();

        // If user has input more than 1 character, try to guess the word
        if (inputString.length() > 1) {
            return this.guessWord(inputString);
        }

        return this.guessLetter(inputString.charAt(0));
    }

    boolean guessWord(String guess) {
        if (guess.equals(word)) {
            unguessedLetters.clear();
            return true;
        }

        return false;
    }

    int getGuessesMade() {
        return guessesMade;
    }

    String getWord() {
        return word;
    }

    int getGuessesLeft() {
        return guessesLeft;
    }

    int getHintsLeft() {
        return hintsLeft;
    }

    void setHintsLeft(int hintsLeft) {
        this.hintsLeft = hintsLeft;
    }


    void setGuessesLeft(int guessesLeft) {
        this.guessesLeft = guessesLeft;
    }
}
