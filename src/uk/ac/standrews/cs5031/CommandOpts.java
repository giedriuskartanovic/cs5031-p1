package uk.ac.standrews.cs5031;

/**
 * A helper class to parse given arguments and provide easy way to retrieve
 * the options that will fallback to defaults if not provided by the user
 */
class CommandOpts {

    int maxGuesses = 10;
    int maxHints = 2;

    String wordSource = "";

    /**
     * @param args Arguments
     */
    CommandOpts(String[] args) {
        for (int i = 0; i < args.length; ++i) {
            switch (args[i]) {
                case "--guesses":
                    maxGuesses = Integer.parseInt(args[i + 1]);
                    i++;
                    break;
                case "--hints":
                    maxHints = Integer.parseInt(args[i + 1]);
                    i++;
                    break;
                default:
                    wordSource = args[i];
                    break;
            }
        }
    }
}
