package uk.ac.standrews.cs5031;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class GameStateTest {

    private final ByteArrayOutputStream output = new ByteArrayOutputStream();

    private final int initialGuesses = 10;
    private final int initialHints = 2;

    private GameState game;

    @Before
    public void setUpStream() {
        System.setOut(new PrintStream(output));
    }

    @After
    public void restoreStream() {
        System.setOut(System.out);
    }

    @Before
    public void setUpGameState() {
        game = new GameState("test", initialGuesses, initialHints);
    }

    @Test
    public void gameStateTest() {
        assertEquals("test", game.getWord());
        assertEquals(initialGuesses, game.getGuessesLeft());
        assertEquals(initialHints, game.getHintsLeft());
        assertEquals(0, game.getGuessesMade());
    }

    @Test
    public void hintTest() {
        assertEquals(initialHints, game.getHintsLeft());
        game.showHint();
        assertEquals(initialHints - 1, game.getHintsLeft());
    }

    @Test
    public void noMoreHintsTest() {
        game.setHintsLeft(0);
        game.showHint();
        assertEquals("No more hints allowed", output.toString().trim());
    }

    @Test
    public void guessesMadeCounterTest() {
        assertEquals(0, game.getGuessesMade());
        game.guessLetter('t');
        assertEquals(1, game.getGuessesMade());
    }

    @Test
    public void guessCorrectLetterTest() {
        assertEquals(initialGuesses, game.getGuessesLeft());
        assertTrue(game.guessLetter('t'));
        assertEquals(initialGuesses, game.getGuessesLeft());
    }

    @Test
    public void guessIncorrectLetterTest() {
        assertEquals(initialGuesses, game.getGuessesLeft());
        assertFalse(game.guessLetter('x'));
        assertEquals((initialGuesses - 1), game.getGuessesLeft());
    }

    @Test
    public void showUnguessedWordTest() {
        game.showWord();
        assertEquals("----", output.toString().trim());
    }

    @Test
    public void showPartiallyGuessedWordTest() {
        game.guessLetter('e');
        game.showWord();
        assertEquals("-e--", output.toString().trim());
    }

    @Test
    public void wonTest() {
        game.guessLetter('t');
        game.guessLetter('e');
        game.guessLetter('s');
        assertTrue(game.won());
    }

    @Test
    public void wonByWordGuessTest() {
        game.guessWord("test");
        assertTrue(game.won());
    }

    @Test
    public void guessWordTest() {
        assertFalse(game.guessWord("wrong"));
        assertTrue(game.guessWord("test"));
    }

    @Test
    public void lostTest() {
        assertFalse(game.lost());
        game.setGuessesLeft(0);
        assertTrue(game.lost());
    }

}
