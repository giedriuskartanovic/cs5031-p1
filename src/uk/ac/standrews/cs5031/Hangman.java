package uk.ac.standrews.cs5031;

import java.util.Scanner;

public class Hangman {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        GameState game;
        CommandOpts opts;
        boolean guessIsCorrect;
        String word = null;

        opts = new CommandOpts(args);

        if (opts.wordSource.equals("")) {
            // No file provided for custom words
            // Allow user to pick one of the pre-defined categories

            while (word == null) {
                try {

                    Words.printCategories();
                    System.out.print("Pick a category:");

                    word = Words.randomWord(sc.nextInt());
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

        } else {
            // Get a random word from the provided custom source
            word = Words.randomWord(opts.wordSource);
        }

        game = new GameState(word, opts.maxGuesses, opts.maxHints);

        while (!game.won() && !game.lost()) {
            game.showWord();
            game.showGuessesRemaining();

            guessIsCorrect = game.getNextGuess();

            if (guessIsCorrect) System.out.println("Good guess!");
            else System.out.println("Wrong guess!");
        }

        if (game.won()) {
            System.out.println("Well done!");
            System.out.println("You took " + game.getGuessesMade() + " guesses");
        } else {
            System.out.println("You lost! The word was " + game.getWord());
        }
    }

}
