package uk.ac.standrews.cs5031;

import java.io.*;
import java.util.ArrayList;

class Words {

    private static String[] wordsCounties =
            {"Argyll and Bute", "Caithness", "Kingdom of Fife",
                    "East Lothian", "Highland", "Dumfries and Galloway",
                    "Renfrewshire", "Scottish Borders", "Perth and Kinross"};

    private static String[] wordsCountries =
            {"Scotland", "England", "Wales", "Northern Ireland", "Ireland",
                    "France", "Germany", "Netherlands", "Spain", "Portugal",
                    "Belgium", "Luxembourg", "Switzerland", "Italy", "Greece"};

    private static String[] wordsCities =
            {"St Andrews", "Edinburgh", "Glasgow", "Kirkcaldy", "Perth",
                    "Dundee", "Stirling", "Inverness", "Aberdeen", "Falkirk"};

    /**
     * Print out possible pre-defined categories
     */
    static void printCategories() {
        System.out.println("1. Counties");
        System.out.println("2. Countries");
        System.out.println("3. Cities");
    }

    /**
     * @param category One of the possible pre-defined categories
     * @return A random word from the picked category
     * @throws Exception When the category picked in not valid
     */
    static String randomWord(int category) throws Exception {
        switch (category) {
            case 1:
                return randomWordFromArray(wordsCounties);
            case 2:
                return randomWordFromArray(wordsCountries);
            case 3:
                return randomWordFromArray(wordsCities);
            default:
                throw new Exception("Unknown category picked");
        }
    }

    /**
     * @param wordSource A path to the file that contains words separated by new line
     * @return A random word from the given source of words
     */
    static String randomWord(String wordSource) {
        String line;
        ArrayList<String> customWords = new ArrayList<>();

        try {
            // Read in the file line-by-line and add each word to the array
            FileReader file = new FileReader(wordSource);
            BufferedReader reader = new BufferedReader(file);
            while ((line = reader.readLine()) != null) {
                customWords.add(line);
            }

            // Return a random word from all the words
            return customWords.get((int) (Math.random() * customWords.size()));
        } catch (FileNotFoundException e) {
            System.out.println("File error");
            return "";
        } catch (IOException e) {
            System.out.println("IO error");
            return "";
        }
    }

    /**
     * @param words An array of words to choose from
     * @return A single random word from the array
     */
    private static String randomWordFromArray(String[] words) {
        return words[(int) (Math.random() * words.length)];
    }
}
